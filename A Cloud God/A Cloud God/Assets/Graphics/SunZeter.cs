﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SunZeter : MonoBehaviour {

	public Vector3 sun;
	public Vector3 sun2;

	// Use this for initialization
	void Awake () {
		sun2 = this.transform.position;

	}
	
	// Update is called once per frame
	void LateUpdate () {
		sun = this.transform.position;
		sun.z = -92;
		this.transform.position = sun;
	}
}
