﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wander : MonoBehaviour {
    public Vector3 steerForce;
    public Vector3 velocity;
	public float mass = 100;
	public Vector3 acceleration;
	public int maxForce = 150;

    public float wanderRadius = 10;
	public float wanderDistance = 1;
	private float wanderJitter;
	public float wanderJitterMin = 0.5f;
	public float wanderJitterMax = 2;
	private float tmrWander = 999;
    public int maxWalkSpeed = 5;

    public Vector3 heading;
    public Vector3 wandertargetPosition;    

    CharacterController controller;

	void Start () {
		controller = GetComponent<CharacterController>();
	}
	
	
	void Update () {
        

		steerForce = Wandering();

       
        Truncate(ref steerForce, maxForce);
        acceleration = steerForce / mass;
        velocity += acceleration;
		Vector3 temp;
		temp = velocity;
		temp.z = 0.0f;
		velocity = temp;
		controller.Move(velocity * Time.deltaTime);

		if(this.transform.position.x>200 || this.transform.position.x<-200 ||  this.transform.position.y>200 || this.transform.position.y<-200)
		{
			this.transform.position = new Vector3 (0.0f, 0.0f, 0.0f);
			velocity = new Vector3 (0.0f, 0.0f, 0.0f);
		}
    }

	public Vector3 Wandering()
	{
		tmrWander += Time.deltaTime;
		if (tmrWander > wanderJitter || Vector3.Distance(wandertargetPosition, transform.position) < 1)
		{
			tmrWander = 0;
			wanderJitter = Random.Range(wanderJitterMin, wanderJitterMax);
			heading = velocity.normalized;
		
			wandertargetPosition = transform.position + transform.forward * wanderDistance + Random.onUnitSphere * wanderRadius;
		
		}
		Vector3 mySteeringForce = (wandertargetPosition - transform.position).normalized * maxForce;
		return mySteeringForce;
	}

    private void Truncate(ref Vector3 myVector, int myMax)
    {
        if (myVector.magnitude > myMax)
        {
            myVector.Normalize();
            myVector *= myMax;
        }
    }
}
