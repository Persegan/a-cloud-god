﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZombieJefe : MonoBehaviour {

	public GameObject jefe;

	public bool alive = true;

	bool underRayo=false;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

		if (underRayo) {
			Destroy(this.gameObject);
		} 

	}

	void OnTriggerEnter(Collider collider)
	{
		if (collider.gameObject.tag == "Rayo") {
			underRayo = true;
		} 
	}

	void OnTriggerExit(Collider collider)
	{
		if (collider.gameObject.tag == "Rayo") {
			underRayo = false;
		} 


	}
}
