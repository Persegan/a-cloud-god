﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Zombie : MonoBehaviour {

	public GameObject jefe;

	bool underRayo=false;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

		if (underRayo) {
			Destroy(this.gameObject);
		} 

		if (jefe == null) {
			GetComponent<Wander> ().enabled=true;
			GetComponent<Flocking> ().enabled=false;
		}
	}

	void OnTriggerEnter(Collider collider)
	{
		if (collider.gameObject.tag == "Rayo") {
			underRayo = true;
		} 
	}

	void OnTriggerExit(Collider collider)
	{
		if (collider.gameObject.tag == "Rayo") {
			underRayo = false;
		} 


	}
}
