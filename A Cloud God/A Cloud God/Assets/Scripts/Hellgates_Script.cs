﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Hellgates_Script : MonoBehaviour {

    public Slider Hellgate_Slider;

    public GameObject hordaZombies;

    public GameObject Gate1;
    public GameObject Gate2;
    public GameObject Gate3;

    public bool gateMaster = false;
    // Use this for initialization
    void Start () {
        Hellgates_Blackboard.blackboard.Humidity = 100;

    }
	
	// Update is called once per frame
	void Update () {

        Hellgate_Slider.value = Hellgates_Blackboard.blackboard.Humidity;

        if (Hellgates_Blackboard.blackboard.Humidity <= 0)
        {
            if (gateMaster == true)
            {
				GameObject temporal = Instantiate(hordaZombies, new Vector3(Gate1.transform.position.x-120,Gate1.transform.position.y+200, 499), Quaternion.identity);
				temporal.GetComponentInChildren<Unit> ().target = GameObject.Find ("Templo").transform;
                GameObject temporaljefe = temporal.GetComponentInChildren<ZombieJefe>().gameObject;

                for (int i = 0; i < 6; i++)
                {
                    temporal.transform.GetChild(i).GetComponent<Zombie>().jefe = temporaljefe;
                }

                temporal = Instantiate(hordaZombies, new Vector3(Gate2.transform.position.x-120,Gate2.transform.position.y+200, 499), Quaternion.identity);
                temporal.GetComponentInChildren<Unit> ().target = GameObject.Find ("Templo").transform;
                temporaljefe = temporal.GetComponentInChildren<ZombieJefe>().gameObject;

                for (int i = 0; i < 6; i++)
                {
                    temporal.transform.GetChild(i).GetComponent<Zombie>().jefe = temporaljefe;
                }


                temporal = Instantiate(hordaZombies, new Vector3(Gate3.transform.position.x-120,Gate3.transform.position.y+200, 499), Quaternion.identity);
				temporal.GetComponentInChildren<Unit> ().target = GameObject.Find ("Templo").transform;
                temporaljefe = temporal.GetComponentInChildren<ZombieJefe>().gameObject;

                for (int i = 0; i < 6; i++)
                {
                    temporal.transform.GetChild(i).GetComponent<Zombie>().jefe = temporaljefe;
                }


                Hellgates_Blackboard.blackboard.Humidity = 100;
            }

        }
		
	}

    void OnTriggerEnter(Collider collider)
    {
        if (collider.gameObject.tag == "Nube")
        {
            Hellgates_Blackboard.blackboard.underRain = true;
        }
        else if (collider.gameObject.tag == "Sol")
        {
            Hellgates_Blackboard.blackboard.underSun = true;
        }

    }

    void OnTriggerExit(Collider collider)
    {
        if (collider.gameObject.tag == "Nube")
        {
            Hellgates_Blackboard.blackboard.underRain = false;
        }
        else if (collider.gameObject.tag == "Sol")
        {
            Hellgates_Blackboard.blackboard.underSun = false;
        }
    }


}
