﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveCloud : MonoBehaviour {

	public float Speed = 0f;
	bool active = false;

	void Update () {
		if (Input.GetKey (KeyCode.Space) && !active) {
			active = true;
		} else {
			active = false;
		}
		if (active) {
			transform.GetChild (0).gameObject.SetActive (true);
		} else {
			transform.GetChild (0).gameObject.SetActive (false);
		}

		if (Input.GetKey (KeyCode.A))
			transform.position -= new Vector3 (Speed - Time.deltaTime, 0.0f, 0.0f);
		if (Input.GetKey (KeyCode.D))
			transform.position += new Vector3 (Speed - Time.deltaTime, 0.0f, 0.0f);
		if (Input.GetKey (KeyCode.W))
			transform.position += new Vector3 (0.0f, Speed - Time.deltaTime, 0.0f);
		if (Input.GetKey (KeyCode.S))
			transform.position -= new Vector3 (0.0f, Speed - Time.deltaTime, 0.0f);
	}


}
