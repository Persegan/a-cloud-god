﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Flocking : MonoBehaviour {
    public Vector3 targetPosition;
    public Vector3 steerForce;
    public float separationDistance = 100;
    public int maxForce = 150;
    public float separationForce = 300;
    public Vector3 velocity;
    public float mass = 100;
    public Vector3 acceleration;
    public float cohesionRadius = 30;
    public float cohesionForce = 20;
    public int maxRunningSpeed = 20;

    CharacterController controller;

    void Start () {
        controller = GetComponent<CharacterController>();
    }
	
	void Update () {

        if (GetComponent<Zombie>().jefe != null)
        {
            targetPosition = GetComponent<Zombie>().jefe.transform.position; targetPosition = GetComponent<Zombie>().jefe.transform.position;
        }

		if (GetComponent<Zombie>().jefe != null) {
			steerForce = Seek(targetPosition);

			steerForce += Separation();
			steerForce += Cohesion();

			Truncate(ref velocity, maxRunningSpeed);
			acceleration = steerForce / mass;
			velocity += acceleration;
			

			Vector3 temp;
			temp = velocity;
			temp.z = 0.0f;
			velocity = temp;

			//Vector3 temp2 = new Vector3(0.0f,0.0f,0.0f);
			

			controller.Move(velocity * Time.deltaTime);
		}
        

    }

	public Vector3 Seek(Vector3 seekPosition)
	{
		Vector3 mySteeringForce = (seekPosition - transform.position).normalized * maxForce;
		Debug.DrawLine(transform.position, seekPosition, Color.green);
		return mySteeringForce;
	}

    public Vector3 Separation()
    {
        Vector3 mySteeringForce = Vector3.zero;
        GameObject[] zombies = GameObject.FindGameObjectsWithTag(transform.tag);
        for (int i = 0; i < zombies.Length; i++)
        {
            
            float dist = Vector3.Distance(transform.position, zombies[i].transform.position);
            if (dist < separationDistance && dist != 0)
            {
                mySteeringForce += (transform.position - zombies[i].transform.position).normalized * separationForce / dist;
            }
        }
        Debug.DrawRay(transform.position, mySteeringForce, Color.cyan);
        return mySteeringForce;
    }

    private void Truncate(ref Vector3 myVector, int myMax)
    {
        if (myVector.magnitude > myMax)
        {
            myVector.Normalize();
            myVector *= myMax;
        }
    }

    private Vector3 Cohesion()
    {
        Vector3 centerOfMass = Vector3.zero;
       
        int neighbours = 0;
        GameObject[] enemies = GameObject.FindGameObjectsWithTag(transform.tag);
        for (int i = 0; i < enemies.Length; i++)
        {
            if (!Physics.Linecast(transform.position, enemies[i].transform.position))
            {
                if (Vector3.Distance(transform.position, enemies[i].transform.position) < cohesionRadius)      
                {
                    centerOfMass += enemies[i].transform.position;
                    neighbours++;
                }
            }
        }
        if (neighbours != 0)
        {
            centerOfMass /= neighbours;
        }
        else 
        {
            centerOfMass = transform.position;
        }
        
        float scaleFactor = cohesionForce * Vector3.Distance(transform.position, centerOfMass);//further away, more pull towards center
        Vector3 mySteeringForce = (centerOfMass - transform.position).normalized * scaleFactor;//look at target direction, normalized and scaled
      
        return mySteeringForce;
    }

}
