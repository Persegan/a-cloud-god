﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hellgates_Blackboard : MonoBehaviour {

   public static Hellgates_Blackboard blackboard;

    public float Humidity;

    public float negativeValor;
    public float plusValor;
    public float multiplicatorValor;
    public bool underRain = false;
    public bool underSun = false;

    void Awake()
    {
        if ( blackboard == null)
        {
            DontDestroyOnLoad(gameObject);
            blackboard = this;
        }
        else if (blackboard != this)
        {
            Destroy(gameObject);
        }
    }

    // Use this for initialization
    void Start () {
        Humidity = 100;
		
	}
	
	// Update is called once per frame
	void Update () {
        if (Humidity > 0 && !underRain)
        {
            Humidity = Humidity - negativeValor * Time.deltaTime;
        }
        else if (underRain && Humidity < 100)
        {
            Humidity = Humidity + plusValor * Time.deltaTime;
        }
        if (underSun && !underRain && Humidity > 0)
        {
            Humidity = Humidity - negativeValor * multiplicatorValor * Time.deltaTime;
        }

    }


}
