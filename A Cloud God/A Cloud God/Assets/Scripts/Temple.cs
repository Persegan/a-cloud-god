﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class Temple : MonoBehaviour {


    public Slider health_slider;
    public float health;
    public float damage;


	// Use this for initialization
	void Start () {

        health = 100;
		
	}
	
	// Update is called once per frame
	void Update () {

       


        health_slider.value = health;
	}

    void OnTriggerEnter(Collider collider)
    {
        if (collider.gameObject.tag == "Respawn")
        {
            Destroy(collider.gameObject);
            health -= damage;
            if (health <= 0)
            {
                SceneManager.LoadScene("GameOver");
            }

        }
		if (collider.gameObject.tag == "ZombieJefe")
		{
			if(collider.gameObject.GetComponent<ZombieJefe>().alive){
				collider.gameObject.GetComponent<ZombieJefe> ().alive = false;
			health -= damage;
			if (health <= 0)
			{
				SceneManager.LoadScene("GameOver");
			}

			}
		}
    }


}
