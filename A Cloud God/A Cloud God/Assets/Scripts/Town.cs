﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Town : MonoBehaviour {

	public GameObject hordaZombies;
	public GameObject town;

    public float Humidity;
    public Slider Humidity_Slider;

	public float negativeValor = 0.005f;
	public float plusValor = 0.05f;
	public float multiplicatorValor = 10f;
	bool underRain = false;
	bool underSun = false;

	int _condition = 0;
	int _action = 1;
	int _sequence = 2;

	struct Child{
		public int type;
		public int numChilds;
		public int actionNum;
		public int conditionNum;

		//condition 0
		//action 1
		//sequence 2
		public Child[] ownChilds;// = new Child[numChilds];

		public void setType(int _type){
			type= _type;
		}
		public void addSequence(int _Size){
			ownChilds= new Child[_Size];
			this.numChilds = _Size;
			this.type = 2;
		}
		public	void addAction(int _actionNum){
			this.actionNum=_actionNum;
			this.type = 1;
		}
		public void addCondition(int _conditionNum){
			this.conditionNum=_conditionNum;
			this.type = 0;
		}

	};
	struct Root
	{
		public int type;
		public int numChilds;
		public Child[] array;//= new Child[numChilds];
	};

	Root Raiz;
	//

	List<System.Action> actions = new List<System.Action>();List<System.Action> conditions = new List<System.Action>();

	bool []arrayBool = new bool[10];
	int _CONDITION_humedadPositiva = 0;

	// Use this for initialization
	void Start () {
		


		Humidity = 100;

		actions.Add(() => ACTION_actualizarHumedad()); int _ACTION_actualizarHumedad = 0;
		actions.Add(() => ACTION_spawnZombies()); int _ACTION_spawnZombies = 1;
		actions.Add(() => ACTION_resetHumidity()); int _ACTION_resetHumidity = 2; 

		Raiz.numChilds = 2;
		Raiz.type = 2;
		Raiz.array = new Child[Raiz.numChilds];
	
		Raiz.array [0].addAction (_ACTION_actualizarHumedad);

		Raiz.array [1].addSequence (3);

		Raiz.array [1].ownChilds [0].addCondition (_CONDITION_humedadPositiva);
		Raiz.array [1].ownChilds [1].addAction (_ACTION_spawnZombies);
		Raiz.array [1].ownChilds [2].addAction (_ACTION_resetHumidity);


	}

	void Update () {
		if (CONDITION_humedadPositiva ()) {
			arrayBool [_CONDITION_humedadPositiva] = true; 
		}
		else 
			arrayBool[0]= false;


		//BT enginge
		if (Raiz.type == _sequence) {

			for (int i = 0; i < Raiz.numChilds; i++) {

				//IF SEQUENCE CHILD
				if (Raiz.array [i].type == _sequence) {
					
					for (int j = 0; j < Raiz.array [i].numChilds; j++) {

						if (Raiz.array [i].ownChilds [j].type == _sequence) {

							//....... SEGUIR AÑADIENDO CODIGO SI QUEREMOS UN BEHAVIOUR TREES MÁS EXTENSO
							// for (...........){
						
												
						}
						if(ifSequenceCondition (Raiz.array [i].ownChilds [j]))break;
					
						ifSequenceAction (Raiz.array [i].ownChilds [j]);
				
						
					}
				}

				ifSequenceAction (Raiz.array [i]);
			
				ifSequenceCondition (Raiz.array [i]);
			}

		}

		//Show Humidity bar
        Humidity_Slider.value = Humidity;		
	}

	void ifSequenceAction (Child child){
		if(child.type==_action)actions [child.actionNum] ();
	}
	bool ifSequenceCondition (Child child){
		if (child.type == _condition) {
			if (!arrayBool [child.conditionNum])
				return true;
		}
			return false;

	}
	/*
	void OnCollisionEnter(Collision collision)
	{
		if (collision.collider.gameObject.tag == "Nube") {
			underRain = true;
		}

	}

	void OnCollisionExit(Collision collision){
		if (collision.collider.gameObject.tag == "Nube") {
			underRain = false;
		}
			
	}
	*/
	void OnTriggerEnter(Collider collider)
	{
		if (collider.gameObject.tag == "Nube") {
			underRain = true;
		} else if (collider.gameObject.tag == "Sol") {
			underSun = true;
		}

	}

	void OnTriggerExit(Collider collider)
	{
		if (collider.gameObject.tag == "Nube") {
			underRain = false;
		}else if (collider.gameObject.tag == "Sol") {
			underSun = false;
		}
	}

	void ACTION_actualizarHumedad(){

		if (Humidity > 0 && !underRain) {
			Humidity = Humidity - negativeValor;
		} else if (underRain && Humidity<100) {
			Humidity = Humidity + plusValor;
		}
		if (underSun && !underRain && Humidity>0) {
			Humidity = Humidity - negativeValor*multiplicatorValor;
		}
	}

	void ACTION_spawnZombies(){
		GameObject temporal = Instantiate(hordaZombies, new Vector3(town.transform.position.x-120,town.transform.position.y+200, 499), Quaternion.identity);
		temporal.GetComponentInChildren<Unit> ().target = GameObject.Find ("Templo").transform;
		GameObject temporaljefe = temporal.GetComponentInChildren<ZombieJefe> ().gameObject;

		for (int i = 0; i < 6; i++) {
			temporal.transform.GetChild (i).GetComponent<Zombie> ().jefe = temporaljefe;
		}

	}

	void ACTION_resetHumidity(){
		Humidity = 100;
	}

	bool CONDITION_humedadPositiva(){
		if (Humidity > 0)
			return false;
		else
			return true;
	}


}
